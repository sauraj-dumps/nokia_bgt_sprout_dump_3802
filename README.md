## qssi-user 11 RKQ1.200928.002 1 release-keys
- Manufacturer: hmd global
- Platform: lito
- Codename: BGT_sprout
- Brand: Nokia
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.200928.002
- Incremental: 00WW_2_270
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: undefined
- Fingerprint: Nokia/BabyGroot_00WW/BGT_sprout:11/RKQ1.200928.002/00WW_2_270:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.200928.002-1-release-keys
- Repo: nokia_bgt_sprout_dump_3802


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
