/*
  Copyright (c) 2018 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
*/

BEGIN TRANSACTION;

INSERT OR REPLACE INTO qcril_properties_table (property, value) VALUES ('qcrildb_version', 9);

DELETE FROM qcril_emergency_source_voice_table where MCC = '502' AND NUMBER = '112';
DELETE FROM qcril_emergency_source_voice_table where MCC = '502' AND NUMBER = '911';
DELETE FROM qcril_emergency_source_voice_table where MCC = '502' AND NUMBER = '991';
DELETE FROM qcril_emergency_source_voice_table where MCC = '502' AND NUMBER = '994';
DELETE FROM qcril_emergency_source_mcc_table where MCC = '724' AND NUMBER = '192';
DELETE FROM qcril_emergency_source_mcc_table where MCC = '724' AND NUMBER = '193';
DELETE FROM qcril_emergency_source_hard_mcc_table where MCC = '724' AND NUMBER = '192';
DELETE FROM qcril_emergency_source_hard_mcc_table where MCC = '724' AND NUMBER = '193';
DELETE FROM qcril_emergency_source_mcc_mnc_table where MCC = '730' AND MNC = '01' AND NUMBER = '131';
DELETE FROM qcril_emergency_source_mcc_mnc_table where MCC = '730' AND MNC = '01' AND NUMBER = '132';

INSERT INTO qcril_emergency_source_voice_table VALUES('605','190','','full');
INSERT INTO qcril_emergency_source_voice_table VALUES('605','193','','full');
INSERT INTO qcril_emergency_source_voice_table VALUES('605','197','','full');
INSERT INTO qcril_emergency_source_voice_table VALUES('605','198','','full');

INSERT INTO qcril_emergency_source_hard_mcc_table VALUES('605','190','','');
INSERT INTO qcril_emergency_source_hard_mcc_table VALUES('605','193','','');
INSERT INTO qcril_emergency_source_hard_mcc_table VALUES('605','197','','');
INSERT INTO qcril_emergency_source_hard_mcc_table VALUES('605','198','','');
INSERT INTO qcril_emergency_source_hard_mcc_table VALUES('732','123','','');

INSERT OR REPLACE INTO qcril_emergency_source_mcc_table VALUES ('452','113','','');
INSERT OR REPLACE INTO qcril_emergency_source_mcc_table VALUES ('452','114','','');
INSERT OR REPLACE INTO qcril_emergency_source_mcc_table VALUES ('452','115','','');
INSERT OR REPLACE INTO qcril_emergency_source_voice_table VALUES('452','113','','full');
INSERT OR REPLACE INTO qcril_emergency_source_voice_table VALUES('452','114','','full');
INSERT OR REPLACE INTO qcril_emergency_source_voice_table VALUES('452','115','','full');

COMMIT TRANSACTION;
